/* global AccountsAvatarField: true */
'use strict';

var avatarFieldServices = {
  google: 'picture',
};

var getAvatarFromService = function(serviceName, service) {
  var servicesName = Object.keys(avatarFieldServices);
  if( _.indexOf(servicesName, serviceName) === -1 ) return;
  var avatarField = avatarFieldServices[serviceName];
  return service[avatarField];
};


var updateAvatar = function(info) {
	var user = info.user;
	var serviceAvatar = null;

	_.each(user.services, function(service, serviceName) {
    if (serviceName === 'resume' ||
      serviceName === 'email' ||
      serviceName === 'password') {
      return;
    }

    serviceAvatar = getAvatarFromService(serviceName, service);
    if (serviceAvatar) return;
  });

	if (serviceAvatar) {
		Meteor.users.update({
			_id: user._id
		}, {
			$set: {
				'profile.avatar': serviceAvatar,
			}
		});
	}
};


var updateAllUsersAvatar = function() {
	Meteor.users.find().forEach(function(user) {
		updateAvatar({
			user: user,
		});
	});
};


AccountsAvatarField = {
  getAvatarFromService: getAvatarFromService,
	updateAllUsersAvatar: updateAllUsersAvatar,
  updateAvatar: updateAvatar,
};

