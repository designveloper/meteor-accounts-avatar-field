'use strict';

Package.describe({
  name: 'nitrolabs:accounts-avatar-field',
  summary: "Find google image_profile",
  version: '0.0.1',
  git: 'git@github.com:Nitrolabs/meteor-accounts-avatar-field.git',
});

Package.onUse((api) => {
  api.versionsFrom('METEOR@1.1');

  api.use([
    'accounts-base@1.2.0',
    'underscore@1.0.3',
  ], ['server']);

  api.imply([
    'accounts-base',
  ], ['server']);

  api.addFiles([
    'lib/_globals.js',
    'lib/accounts-avatar-field.js',
    'lib/accounts-avatar-field-on-login.js',
  ], ['server']);

  api.export([
    'AccountsAvatarField',
  ], ['server']);
});

Package.onTest((api) => {
  api.use('nitrolabs:accounts-avatar-field');

  api.use([
    'tinytest',
    'test-helpers',
    'underscore',
  ], ['server']);
});
